﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CSCI257_RJM_task1.Models;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Web.Http;
using System.Net;
using System.Net.Http;
using RouteAttribute = System.Web.Http.RouteAttribute;

namespace CSCI257_RJM_task1.Controllers
{
    public class AccessoryController : ApiController
    {
        private Context db = new Context();
        // GET: api/Accessory
        public IEnumerable<Accessory> GetAccessories()
        {
            return db.Accessories.AsEnumerable();
        }

        [Route("api/platform/{platformId}/accessory", Name = "GetAccessoriesForPlatform")]
        public IEnumerable<Accessory> GetAccessoriesForPlatform(int platformId)
        {
            return db.Accessories.Where(x => x.Platform.PlatformId == platformId).AsEnumerable();
        }

        // GET api/accessory/5
        public Accessory GetAccessory(int id)
        {
            Accessory accessory = db.Accessories.Find(id);
            if (accessory == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return accessory;
        }

        //Get one accessory by platform
        [Route("api/platform/{platformId}/accessory/{id}")]
        public Accessory GetAccessoryByPlatform(int platformId, int id)
        {
            Accessory accessory = db.Accessories.Where(x => x.Platform.PlatformId == platformId && x.AccessoryId == id).Include(p => p.Platform).FirstOrDefault();
            if (accessory == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return accessory;
        }

        // POST api/accessory
        public HttpResponseMessage PostAccessory(Accessory accessory)
        {
            if (ModelState.IsValid)
            {
                db.Accessories.Add(accessory);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, accessory);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = accessory.AccessoryId }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        //Post new accessory by platform
        [Route("api/platform/{platformId}/accessory", Name = "PostAccessoryForPlatform")]
        public HttpResponseMessage PostAccessoryForPlatform(Accessory accessory, int platformId)
        {
            if (ModelState.IsValid)
            {
                Platform platform = db.Platforms.Find(platformId);
                if (platform == null)
                {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
                }
                accessory.Platform = db.Platforms.Find(platformId);
                db.Accessories.Add(accessory);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, accessory);
                //response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = product.ProductId }));
                //response.Headers.Location = new Uri(Url.Link("GetAccessoriesForPlatform", new { platformId = platform.PlatformId }));


                //CreatedAtRoute("DefaultApi", new { controller = "messages", id = message.Id }, message);
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // PUT api/accessory/5
        public HttpResponseMessage PutAccessory(int id, Accessory accessory)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != accessory.AccessoryId)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(accessory).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        //Put to modify accessory by platform
        [Route("api/platform/{platformId}/accessory/{id}", Name = "PutAccessoryForPlatform")]
        public HttpResponseMessage PutStaffMemberForDepartment(Accessory acc, int platformId, int id)
        {
            if (ModelState.IsValid)
            {
                Platform platform = db.Platforms.Find(platformId);
                if (platform == null)
                {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
                }
                Accessory accessory = db.Accessories.Where(s => s.AccessoryId == id).Include(s => s.Platform).FirstOrDefault();
                if (accessory == null)
                {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
                }
                if (acc.Platform.PlatformId == platformId)
                {
                    acc.Name = acc.Name;
                    acc.Type = acc.Type;
                    acc.Condition = acc.Condition;
                    acc.Platform = acc.Platform;
                    db.SaveChanges();
                }


                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, acc);
                //response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = product.ProductId }));
                response.Headers.Location = new Uri(Url.Link("GetAccessoryForPlatform", new { platformId = platform.PlatformId }));


                //CreatedAtRoute("DefaultApi", new { controller = "messages", id = message.Id }, message);
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/accessory/5
        public HttpResponseMessage DeleteAccessory(int id)
        {
            Accessory accessory = db.Accessories.Find(id);
            if (accessory == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Accessories.Remove(accessory);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, accessory);
        }
    }
}