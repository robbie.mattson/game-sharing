﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CSCI257_RJM_task1.Models;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Web.Http;
using System.Net;
using System.Net.Http;

namespace CSCI257_RJM_task1.Controllers
{
    public class PlatformController : ApiController
    {
        private Context db = new Context();

        // GET: api/Platform
        public IEnumerable<Platform> GetPlatforms()
        {
            return db.Platforms.AsEnumerable();
        }

        // GET api/platform/5
        public Platform GetPlatform(int id)
        {
            Platform platform = db.Platforms.Find(id);
            if (platform == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return platform;
        }

        // POST api/platform
        public HttpResponseMessage PostPlatform(Platform platform)
        {
            if (ModelState.IsValid)
            {
                db.Platforms.Add(platform);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, platform);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = platform.PlatformId }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // PUT api/platform/5
        public HttpResponseMessage PutPlatform(int id, Platform platform)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != platform.PlatformId)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(platform).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // DELETE api/videogame/5
        public HttpResponseMessage DeleteVideoGame(int id)
        {
            Platform platform = db.Platforms.Find(id);
            if (platform == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Platforms.Remove(platform);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, platform);
        }

        // DELETE api/accessory/5
        public HttpResponseMessage DeleteAccessory(int id)
        {
            Accessory accessory = db.Accessories.Find(id);
            if (accessory == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Accessories.Remove(accessory);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, accessory);
        }
    }
}