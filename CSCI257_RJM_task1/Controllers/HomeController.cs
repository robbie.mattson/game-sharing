﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CSCI257_RJM_task1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult Accessories()
        {
            ViewBag.Title = "Accessories Page";

            return View();
        }

        public ActionResult GameSharing()
        {
            ViewBag.Title = "GameSharing Page";

            return View();
        }
    }
}
