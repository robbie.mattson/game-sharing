﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CSCI257_RJM_task1.Models;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Web.Http;
using System.Net;
using System.Net.Http;
//using RouteAttribute = System.Web.Mvc.RouteAttribute;
using RouteAttribute = System.Web.Http.RouteAttribute;

namespace CSCI257_RJM_task1.Controllers
{
    public class VideoGameController : ApiController
    {
        private Context db = new Context();
        // GET: api/VideoGame
        public List<VideoGame> GetVideoGames()
        {
            return db.VideoGames.ToList();
        }

        [Route("api/platform/{platformId}/videogame", Name = "GetVideoGamesForPlatform")]
        public IEnumerable<VideoGame> GetVideoGamesForPlatform(int platformId)
        {
            //var items = db.VideoGames.Where(x => x.Platform.PlatformId == platformId).Include(p => p.Platform).AsEnumerable();
            var items = db.VideoGames.Where(x => x.Platform.PlatformId == platformId).AsEnumerable();
            var itemslist = items.ToList();
            return items;
        }

        // GET api/videogame/5
        public VideoGame GetVideoGame(int id)
        {
            VideoGame videogame = db.VideoGames.Find(id);
            if (videogame == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return videogame;
        }

        //Get one videogame by platform
        [Route("api/platform/{platformId}/videogame/{id}")]
        public VideoGame GetVideoGameByPlatform(int platformId, int id)
        {
            VideoGame videogame = db.VideoGames.Where(x => x.Platform.PlatformId == platformId && x.VideoGameId == id).Include(p => p.Platform).FirstOrDefault();
            if (videogame == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return videogame;
        }

        //Get videogames borrowed by user
        [Route("api/user/{userId}/borrowedgames/")]
        public List<VideoGame> GetVideoGamesBorrowedByUser(int userId)
        {
            var games = db.VideoGames.Where(x => x.BorrowedBy.UserId == userId && x.Owner.UserId != userId);
            if (games == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return games.ToList();
        }

        //Get videogames availables from user
        [Route("api/user/{userId}/availablegames/")]
        public IEnumerable<VideoGame> GetGamesAvailableByUser(int userId)
        {
            var games = db.VideoGames.Where(x => x.BorrowedBy.UserId == userId && x.Owner.UserId == userId).ToList();
            if (games == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return games;
        }

        // POST api/videogame
        public HttpResponseMessage PostVideoGame(VideoGame videogame)
        {
            if (ModelState.IsValid)
            {
                db.VideoGames.Add(videogame);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, videogame);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = videogame.VideoGameId }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        //Post new videogame by platform
        [Route("api/platform/{platformId}/videogame/{userId}", Name = "PostVideoGameForPlatform")]
        public HttpResponseMessage PostVideoGameForPlatform(VideoGame videoGame, int platformId, int userId)
        {
            if (ModelState.IsValid)
            {
                Platform platform = db.Platforms.Find(platformId);
                User user = db.Users.Find(userId);
                if (platform == null || user == null)
                {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
                }
                
                videoGame.Platform = platform;
                videoGame.Owner = user;
                videoGame.BorrowedBy = user;
                db.VideoGames.Add(videoGame);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, videoGame);
                //response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = product.ProductId }));
                //response.Headers.Location = new Uri(Url.Link("GetVideoGamesForPlatform", new { platformId = platform.PlatformId }));


                //CreatedAtRoute("DefaultApi", new { controller = "messages", id = message.Id }, message);
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // PUT api/videogame/5
        public HttpResponseMessage PutVideoGame(int id, VideoGame videogame)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != videogame.VideoGameId)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(videogame).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        //Put to modify videogame by platform
        [Route("api/platform/{platformId}/videogame/{id}", Name = "PutVideoGameForPlatform")]
        public HttpResponseMessage PutVideoGameForPlatform(VideoGame videoGame, int platformId, int id)
        {
            if (ModelState.IsValid)
            {
                Platform platform = db.Platforms.Find(platformId);
                if (platform == null)
                {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
                }
                VideoGame videogame = db.VideoGames.Where(s => s.VideoGameId == id).Include(s => s.Platform).FirstOrDefault();
                if (videogame == null)
                {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
                }
                if (videogame.Platform.PlatformId == platformId)
                {
                    videogame.Title = videoGame.Title;
                    videogame.Condition = videoGame.Condition;
                    videogame.Platform = videoGame.Platform;
                    db.SaveChanges();
                }


                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, videoGame);
                //response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = product.ProductId }));
                response.Headers.Location = new Uri(Url.Link("GetVideoGameForPlatform", new { platformId = platform.PlatformId }));


                //CreatedAtRoute("DefaultApi", new { controller = "messages", id = message.Id }, message);
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        //Put to return videogame to owner
        [Route("api/videogame/{id}/return/", Name = "PutReturnGameToOwner")]
        public HttpResponseMessage PutReturnGameToOwner(VideoGame videoGame, int id)
        {
            if (ModelState.IsValid)
            {
                VideoGame videogame = db.VideoGames.Where(s => s.VideoGameId == id).FirstOrDefault();
                if (videogame == null)
                {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
                }

                videogame.BorrowedBy = videogame.Owner;
                db.SaveChanges();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, videoGame);
                //response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = product.ProductId }));
                //response.Headers.Location = new Uri(Url.Link("PutReturnGameToOwner", new { platformId = platform.PlatformId }));


                //CreatedAtRoute("DefaultApi", new { controller = "messages", id = message.Id }, message);
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        //Put to borrow videogame
        [Route("api/videogame/{id}/borrow/{userBorrowersId}", Name = "PutBorrowVideoGame")]
        public HttpResponseMessage PutBorrowVideoGame(VideoGame videoGame, int id, int userBorrowersId)
        {
            if (ModelState.IsValid)
            {

                var user = db.Users.Find(userBorrowersId);
                
                VideoGame videogame = db.VideoGames.Where(s => s.VideoGameId == id).Include(u => u.Owner).FirstOrDefault();
                if (videogame == null || user == null)
                {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
                }

                videogame.BorrowedBy = user;
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, videoGame);
                //response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = product.ProductId }));
                //response.Headers.Location = new Uri(Url.Link("PutReturnGameToOwner", new { platformId = platform.PlatformId }));


                //CreatedAtRoute("DefaultApi", new { controller = "messages", id = message.Id }, message);
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/videogame/5
        public HttpResponseMessage DeleteVideoGame(int id)
        {
            VideoGame videogame = db.VideoGames.Find(id);
            if (videogame == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.VideoGames.Remove(videogame);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, videogame);
        }
    }
}