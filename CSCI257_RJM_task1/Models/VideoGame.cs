﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CSCI257_RJM_task1.Models
{
    public class VideoGame
    {
        [Key]
        public int VideoGameId { get; set; }

        [Required]
        [MaxLength(30)]
        public string Title { get; set; }

        [Required]
        [MaxLength(30)]
        public string Condition { get; set; }
        public Platform Platform { get; set; }

        public virtual User Owner { get; set; }

        public virtual User BorrowedBy { get; set; }

        public override string ToString()
        {
            return string.Format("VideoGame: ID:{0}, Title: {1}, Condition: {2}, Platform: {3}", VideoGameId, Title, Condition, Platform);
        }
    }
}