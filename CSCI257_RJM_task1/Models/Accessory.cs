﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CSCI257_RJM_task1.Models
{
    public class Accessory
    {
        [Key]
        public int AccessoryId { get; set; }

        [Required]
        [MaxLength(30)]
        public string Name { get; set; }

        [Required]
        [MaxLength(30)]
        public string Type { get; set; }

        [Required]
        [MaxLength(30)]
        public string Condition { get; set; }
        public Platform Platform { get; set; }

        public override string ToString()
        {
            return string.Format("Accessory: ID:{0}, Name: {1}, Type: {2}, Condition: {3}, Platform: {4}", AccessoryId, Name, Type, Condition, Platform);
        }
    }
}