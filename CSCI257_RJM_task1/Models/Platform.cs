﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CSCI257_RJM_task1.Models
{
    public class Platform
    {
        [Key]
        public int PlatformId { get; set; }

        [Required]
        [MaxLength(30)]
        public string Name { get; set; }

        [Required]
        [MaxLength(30)]
        public string Model { get; set; }

        [Required]
        [MaxLength(30)]
        public string Condition { get; set; }

        public List<VideoGame> VideoGames { get; set; } = new List<VideoGame>();

        public List<Accessory> Accessories { get; set; } = new List<Accessory>();

        public override string ToString()
        {
            return string.Format("Platform: ID:{0}, Name: {1}, Model: {2}, Condition: {3}", PlatformId, Name, Model, Condition);
        }
    }
}