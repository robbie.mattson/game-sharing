﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSCI257_RJM_task1.Models
{
    public class User
    {
        [Key]
        public int UserId { get; set; }
        [Required]
        [MaxLength(30)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(30)]
        public string LastName { get; set; }

        [InverseProperty("Owner")]
        public List<VideoGame> OwnedVideoGames { get; set; } = new List<VideoGame>();

        [InverseProperty("BorrowedBy")]
        public List<VideoGame> BorrowedVideoGames { get; set; } = new List<VideoGame>();

        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }
    }
}