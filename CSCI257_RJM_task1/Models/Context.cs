﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace CSCI257_RJM_task1.Models
{
    public class Context : DbContext
    {
        public Context()
        : base("ConnectionString")
        { }

        public DbSet<VideoGame> VideoGames { get; set; }
        public DbSet<Accessory> Accessories { get; set; }
        public DbSet<Platform> Platforms { get; set; }
        public DbSet<User> Users { get; set; }
        //protected override void OnModelCreating(System.Data.Entity.ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Owner>()
        //   .HasMany(o => o.Vehicles)
        //   .WithOne(v => v.Owner)
        //   .HasForeignKey(v => v.OwnerId);




        //    modelBuilder.Entity<Owner>()
        //    .HasOne(o => o.Address)
        //    .WithOne(a => a.Owner)
        //    .HasForeignKey<Owner>(a => a.AddressId);
        //}

        //select user
        //see games they have borrowed
        //select game and return it

        //select user who is borrowing
        //games availible to borrow
        //select user to borrow from
        //games where user and owner are the same
        //select

        //update index to reflect updated videogame class
    }
}