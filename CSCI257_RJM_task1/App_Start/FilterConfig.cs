﻿using System.Web;
using System.Web.Mvc;

namespace CSCI257_RJM_task1
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
